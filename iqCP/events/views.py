from datetime import datetime, timedelta

from django.http import Http404
from django.shortcuts import render
from django.contrib.auth.decorators import login_required
from django.contrib.auth.mixins import LoginRequiredMixin
from django.views.decorators.http import require_http_methods
from django.views.generic import ListView
from django.db.models import Count
from django.db.models.functions import TruncMonth, TruncDay, TruncYear

from django.core.paginator import Paginator, EmptyPage, PageNotAnInteger

from .models import Lessons, Streams

# Create your views here.

class EventsStat(LoginRequiredMixin, ListView):

    _accepted_group_by = ('month', 'year', 'day')
    _default_group_by = 'month'

    template_name = 'events/stat.html'
    context_object_name = 'events'

    paginate_by = 25

    def get_queryset(self):
        group_by = self.request.GET.get('group_by')

        if group_by not in self._accepted_group_by:
            group_by = self._default_group_by
        self.group_by = group_by

        events = Lessons.objects.all()
        if group_by == 'month':
            trunc = TruncMonth
        elif group_by == 'day':
            trunc = TruncDay
        elif group_by == 'year':
            trunc = TruncYear
        else:
            raise Http404('Нет такой страницы')

        events = events.annotate(date=trunc('start_time')).\
            values('date').\
            annotate(count=Count('date')).\
            order_by('-date')

        return events

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)

        context['group_by'] = self.group_by

        all_events = Lessons.objects.count()
        now = datetime.utcnow()
        yesterday = (now - timedelta(1)).date()
        today_events = Lessons.objects.filter(start_time__gte=now.date()).count()
        events_last_day = Lessons.objects.filter(start_time__gte=yesterday).count()

        context['all_events'] = all_events
        context['today_events'] = today_events
        context['events_last_day'] = events_last_day

        return context

@login_required()
@require_http_methods(('GET', ))
def events_stat(request):

    all_events = Lessons.objects.count()
    now = datetime.utcnow()
    yesterday = (now - timedelta(1)).date()
    today_events = Lessons.objects.filter(start_time__gte=now.date()).count()
    events_last_day = Lessons.objects.filter(start_time__gte=yesterday).count()

    events_by_months = Lessons.objects.\
        annotate(month=TruncMonth('start_time')).\
        values('month').\
        annotate(count=Count('month')).\
        order_by('-month')

    print(events_by_months)

    return render(request, 'events/stat.html', {
        'all_events': all_events,
        'today_events': today_events,
        'events_last_day': events_last_day,
        'events_by_months': events_by_months,
    })