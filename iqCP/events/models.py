from django.db import models
from users.models import Users

# Create your models here.


class Lessons(models.Model):
    lesson_id = models.AutoField(primary_key=True)
    room = models.ForeignKey('Rooms', models.DO_NOTHING)
    subject = models.ForeignKey('Subjects', models.DO_NOTHING, blank=True, null=True)
    description = models.TextField(blank=True, null=True)
    start_time = models.DateTimeField(blank=True, null=True)
    end_time = models.DateTimeField(blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'lessons'


class Streams(models.Model):
    stream_id = models.AutoField(primary_key=True)
    room = models.ForeignKey('Rooms', models.DO_NOTHING, blank=True, null=True)
    test = models.ForeignKey('Tests', models.DO_NOTHING)
    add_time = models.DateTimeField(blank=True, null=True)
    del_time = models.DateTimeField(blank=True, null=True)
    del_reason = models.TextField(blank=True, null=True)  # This field type is a guess.
    override = models.ForeignKey('Override', models.DO_NOTHING, blank=True, null=True)
    pace_mode = models.SmallIntegerField(blank=True, null=True)
    lesson = models.ForeignKey('Lessons', models.DO_NOTHING, blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'streams'


class Override(models.Model):
    override_id = models.AutoField(primary_key=True)
    room_id = models.IntegerField()
    test_id = models.IntegerField()
    position = models.SmallIntegerField(blank=True, null=True)
    timer = models.SmallIntegerField(blank=True, null=True)
    random = models.NullBooleanField()
    used = models.NullBooleanField()
    deleted = models.NullBooleanField()

    class Meta:
        managed = False
        db_table = 'override'


class Rooms(models.Model):
    room_id = models.AutoField(primary_key=True)
    owner = models.ForeignKey(Users, models.DO_NOTHING)
    room_pass = models.CharField(max_length=100, blank=True, null=True)
    room_access = models.SmallIntegerField()
    room_status = models.BooleanField()
    queue = models.TextField(blank=True, null=True)  # This field type is a guess.
    show_results = models.NullBooleanField()

    class Meta:
        managed = False
        db_table = 'rooms'


class Subjects(models.Model):
    subject_id = models.AutoField(primary_key=True)
    title = models.CharField(max_length=100)

    class Meta:
        managed = False
        db_table = 'subjects'

class Tests(models.Model):
    test_id = models.AutoField(primary_key=True)
    test_c = models.ForeignKey('TestsC', models.DO_NOTHING, blank=True, null=True)
    branch = models.ForeignKey('Branches', models.DO_NOTHING, blank=True, null=True)
    positions = models.TextField(blank=True, null=True)  # This field type is a guess.
    creation_time = models.DateTimeField(blank=True, null=True)
    deleted = models.DateTimeField(blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'tests'


class TestsC(models.Model):
    test_c_id = models.AutoField(primary_key=True)
    test_description = models.TextField(blank=True, null=True)
    test_random = models.NullBooleanField()
    test_timer = models.IntegerField(blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'tests_c'


class Branches(models.Model):
    branch_id = models.AutoField(primary_key=True)
    owner_id = models.IntegerField()
    branch_type = models.SmallIntegerField()
    current = models.IntegerField(blank=True, null=True)
    branch_name = models.CharField(max_length=150, blank=True, null=True)
    creation_time = models.DateTimeField(blank=True, null=True)
    editing_time = models.DateTimeField(blank=True, null=True)
    deleted = models.DateTimeField(blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'branches'