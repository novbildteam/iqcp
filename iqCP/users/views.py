from django.contrib.auth.mixins import LoginRequiredMixin
from django.views.generic import ListView, DetailView
from django.db.models import Q
from django import forms
from django.views.generic.edit import FormMixin


from .models import Users, UserProfile, UsersRooms
from events.models import Lessons


class UsersListForm(forms.Form):
    accepted_choices = (
        ('', 'все'),
        (0, 'пользователи'),
        (1, 'участники (студенты)')
    )
    search = forms.CharField(label='Поиск',
                             required=False,
                             initial='')
    user_type = forms.ChoiceField(widget=forms.RadioSelect,
                                  choices=accepted_choices,
                                  initial='',
                                  required=False)


class UsersList(LoginRequiredMixin, ListView, FormMixin):

    queryset = UserProfile.objects.prefetch_related('user').order_by('-user__user_reg_time')
    template_name = 'users/users_list.html'
    context_object_name = 'users'
    paginate_by = 25

    success_url = '.'
    form_class = UsersListForm


    def get_queryset(self):
        users = self.queryset

        form = self.get_form()

        is_valid = form.is_valid()
        if is_valid:

            # filter by user_type
            user_type = form.cleaned_data.get('user_type')
            if user_type:
                users = users.filter(user__type=user_type)

            # filter by search field
            search = form.cleaned_data['search']

            if search:
                search_q = Q(profile_first_name__icontains=search) | Q(profile_second_name__icontains=search)
                # TODO: need full text search instead. I will fix it later
                for s in search.split(' '):
                    search_q |= Q(profile_first_name__icontains=s) | Q(profile_second_name__icontains=s)
                users = users.filter(search_q)

        return users

    def get_form_kwargs(self):
        kwargs = super().get_form_kwargs()
        if self.request.GET:
            kwargs.update({'data': self.request.GET})
        return kwargs


class UserDetail(DetailView):
    queryset = Users.objects.all()
    template_name = 'users/detail.html'
    context_object_name = 'user_detail'

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context['user_profile'] = UserProfile.objects.get(user=self.object)

        if self.object.type == 0:  # user (admin)
            created_events = Lessons.objects.select_related('room').filter(room__owner=self.object).count()
            context['created_events'] = created_events
        participated_events = UsersRooms.objects.filter(user=self.object).count()
        context['participated_events'] = participated_events

        return context




