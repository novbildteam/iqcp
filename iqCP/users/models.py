from django.db import models

# Create your models here.

class Users(models.Model):
    user_id = models.AutoField(primary_key=True)
    user_login = models.CharField(unique=True, max_length=100)
    user_pass = models.CharField(max_length=100)
    user_salt = models.CharField(max_length=100)
    user_reg_time = models.DateTimeField(blank=True, null=True)
    user_login_time = models.DateTimeField(blank=True, null=True)
    type = models.SmallIntegerField(blank=True, null=True)
    last_activity_time = models.DateTimeField(blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'users'


class UserProfile(models.Model):
    user = models.ForeignKey('Users', models.DO_NOTHING, primary_key=True)
    profile_first_name = models.CharField(max_length=100, blank=True, null=True)
    profile_second_name = models.CharField(max_length=100, blank=True, null=True)
    profile_description = models.CharField(max_length=200, blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'user_profile'


class UsersRooms(models.Model):
    working_user_id = models.AutoField(primary_key=True)
    user = models.ForeignKey(Users, models.DO_NOTHING, blank=True, null=True)
    first_name = models.CharField(max_length=100, blank=True, null=True)
    room_id = models.IntegerField()
    con_time = models.DateTimeField(blank=True, null=True)
    discon_time = models.DateTimeField(blank=True, null=True)
    second_name = models.CharField(max_length=100, blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'users_rooms'


