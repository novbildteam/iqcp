from django.conf.urls import url

from . import views

app_name = 'users'
urlpatterns = [
    url(r'^list/$', views.UsersList.as_view(), name='list'),
    url(r'^detail/(?P<pk>[0-9]+)$', views.UserDetail.as_view(), name='detail')
]