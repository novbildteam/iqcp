import os
from django.shortcuts import render
from django.core.urlresolvers import reverse, reverse_lazy
from django.views import generic
from django.template.loader import get_template
from django.contrib.auth.decorators import login_required
from django.contrib.auth.mixins import LoginRequiredMixin
from django.core.paginator import Paginator, EmptyPage, PageNotAnInteger
from django.core.mail import send_mail, get_connection
from django.views.generic import ListView, DetailView
from django.views.generic.edit import UpdateView, DeleteView, FormMixin
from django import forms
from django.db.models import F, Value, Count
from django.db.models.functions import Concat

from smtplib import SMTPAuthenticationError, SMTPException


from django.http import HttpResponseRedirect, HttpResponse


from .helpers import generate_promo, InviteEmailMessage
from .models import PromoCodes, SiteSubscribers
from accounts.models import Profile
from .forms import *

#форма редактирования promo codes detail
class PromoCodeDetailForm(forms.Form):
    comment = forms.CharField(label='Комментарий')

class ListPromoCodes(generic.ListView):
    model = PromoCodes
    template_name = 'promo/list_promo_codes.html'
    context_object_name = 'promo_codes_list'

    def get_queryset(self):
        return PromoCodes.objects.order_by('pk').all()


class PromoCodesListView(LoginRequiredMixin, ListView):

    template_name = 'promo/list_promo_codes.html'
    context_object_name = 'promo_codes'
    queryset = PromoCodes.objects.order_by('pk')\
        .select_related('subscriber')\
        .prefetch_related('userspromo_set__user__userprofile_set') \
        .annotate(user_id=F('userspromo__user__user_id'))\
        .annotate(profile_first_name=F('userspromo__user__userprofile__profile_first_name')) \
        .annotate(profile_second_name=F('userspromo__user__userprofile__profile_second_name')) \
        .all()

    paginate_by = 25

    def get_queryset(self):
        return self.queryset


class PromoCodeDetailView(LoginRequiredMixin, UpdateView):
    model = PromoCodes
    queryset = PromoCodes.objects
    context_object_name = 'promo_code'
    template_name = 'promo/promo_code_detail.html'

    fields = ['comment', ]


class PromoCodeDelete(LoginRequiredMixin, DeleteView):
    model = PromoCodes
    success_url = reverse_lazy('promo:promo_codes')
    http_method_names = ['post', ]


@login_required()
def promoCodeDetail(request, id):
    try:
        promo_code = PromoCodes.objects.get(pk=id)
    except:
        promo_code = None
    form = None
    if promo_code:
        if request.method == 'POST':
            form = PromoCodeDetailForm(request.POST)
            if form.is_valid():
                promo_code.comment = form.cleaned_data['comment']
                promo_code.save()
                return render(request, 'promo/promo_code_detail_success.html', {
                    'promo_code': promo_code,
                    'back_url': request.META.get('HTTP_REFERER')
                })
        else:
            form = PromoCodeDetailForm(initial={'comment': promo_code.comment})
    return render(request, 'promo/promo_code_detail.html', {'promo_code': promo_code, 'form': form})


@login_required()
def deletePromoCode(request, id):
    try:
        promo_code = PromoCodes.objects.get(pk=id)
    except:
        promo_code = None
        return HttpResponseRedirect(reverse('promo:promo_codes'))
    if request.method == 'POST':
        is_del = promo_code.delete()
        if is_del[0] == 1:
            return render(request, 'promo/promo_code_deleted.html', {'promo_code': promo_code})
        else:
            return render(request, 'promo/promo_code_deleted.html', {'error': True})
    else:
        return render(request, 'promo/promo_code_delete.html', {'promo_code': promo_code})


@login_required()
def invite_view(request, promo_code='', first_name='', email='', subscriber_id=None):
    profile = Profile.objects.filter(user=request.user)
    if request.method == 'POST':
        form = SendInviteForm(request.POST)
        if form.is_valid():
            if form.cleaned_data['promo_code']:
                promo_c = form.cleaned_data['promo_code']
            else:
                promo_c = generate_promo(comment=form.cleaned_data['recipient']+','+form.cleaned_data['name'])
            if not promo_c:
                return HttpResponse('error in promo code!')
            try:
                sender = form.cleaned_data['sender']
                with get_connection(
                        host=sender.smtp_server,
                        port=sender.smtp_port,
                        username=sender.email,
                        password=sender.email_password,
                        use_ssl=(True if sender.secure_type == 1 else False)
                ) as connection:
                    email_instance = InviteEmailMessage(template_params={
                                                            'promo_code': promo_c,
                                                            'name': form.cleaned_data['name']
                                                            },
                                                        from_email=sender.email,
                                                        to=[form.cleaned_data['recipient'],],
                                                        connection=connection,
                    )
                    emails = email_instance.send()
                    if emails > 0:
                        if subscriber_id:
                            promo_obj = PromoCodes.objects.get(code=promo_c)
                            promo_obj.subscriber_id = subscriber_id
                            promo_obj.save()
                        return render(request, 'promo/invited.html')
                    else:
                        return HttpResponse('inviting error!')
            except SMTPAuthenticationError:
                return HttpResponse('authentication error!')
            except SMTPException:
                return HttpResponse('SMTP error!')
    else:
        if subscriber_id:
            try:
                subscriber = SiteSubscribers.objects.get(subscriber_id=subscriber_id)
                first_name = subscriber.first_name
                email = subscriber.email
            except:
                pass
        print('e',first_name,email)
        form = SendInviteForm(initial={
            'promo_code': promo_code,
            'name': first_name,
            'recipient': email,
        })
    preview_template = InviteEmailMessage.get_template({'preview': True})
    return render(request, 'promo/invite_view.html',
                  {'form': form, 'template': preview_template,
                   'promo_code': promo_code, 'first_name':first_name,
                   'email': email})


class SubscribersViewForm(forms.Form):
    search = forms.CharField(label='Поиск',
                             required=False,
                             initial='')



class SubscribersView(LoginRequiredMixin, ListView, FormMixin):

    queryset = SiteSubscribers.objects.order_by('-time').prefetch_related('promocodes_set').all()
    template_name = 'promo/subscribers.html'
    context_object_name = 'subscribers'
    paginate_by = 25

    form_class = SubscribersViewForm

    def get_queryset(self):
        subscribers = self.queryset

        form = self.get_form()

        if form.is_valid():
            search = form.cleaned_data.get('search')
            if search:
                subscribers = subscribers.annotate(fullname=Concat(F('first_name'), Value(' '), F('second_name')))\
                    .filter(fullname__icontains=search)

        return subscribers

    def get_form_kwargs(self):
        kwargs = super().get_form_kwargs()
        if self.request.GET:
            kwargs.update({'data': self.request.GET})
        return kwargs