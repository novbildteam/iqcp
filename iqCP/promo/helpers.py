import random, os
from datetime import datetime
from django.core.mail import EmailMessage
from django.template.loader import get_template as get_temp
from iqCP.settings import BASE_DIR, STATIC_URL

from email.mime.base import MIMEBase
from email.encoders import encode_base64

from .models import PromoCodes


def generate_promo(comment=''):
    limits = (10000, 99999)
    promo_codes = PromoCodes.objects.values_list('code', flat=True)
    if len(promo_codes) == limits[1] - limits[0]:
        return False
    stop = False
    while not stop:
        promo_code = str(random.randint(limits[0], limits[1]))
        if promo_code not in promo_codes:
            stop = True
    new_promo = PromoCodes(code=promo_code, comment=comment, creation_time=datetime.now())
    new_promo.save()
    return promo_code


class InviteEmailMessage(EmailMessage):
    template_path = 'emails/invite_promo_template.html'
    basepath = os.path.join(STATIC_URL, 'emails')
    default_template_params = {
        'promo_code': '',
        'name': '',
        'banner': 'banner.png',
        'mobile': 'mobile.png',
        'quiz': 'quiz.png',
        'visual': 'visual.png',
        'share': 'share.png',
        'basepath': basepath,
    }




    def __init__(self, template_params=default_template_params, from_email=None, to=None, bcc=None,
                 connection=None, attachments=None, headers=None, cc=None,
                 reply_to=None):

        EmailMessage.__init__(self, subject='Добро пожаловать в InstaQuiz',
                              body=self.get_template(template_params),
                              from_email=from_email, to=to, bcc=bcc,
                              connection=connection, attachments=attachments, headers=headers, cc=cc,
                              reply_to=reply_to)
        render_params = {k: v for k, v in self.default_template_params.items()}
        for k, v in template_params.items():
            render_params[k] = v
        self.content_subtype = 'html'
        self.attach_image(render_params['banner'])
        self.attach_image(render_params['quiz'])
        self.attach_image(render_params['mobile'])
        self.attach_image(render_params['visual'])
        self.attach_image(render_params['share'])

    def attach_image(self, path):
        path = os.path.join(BASE_DIR, 'static', 'emails', path)
        attach = MIMEBase('image', 'png')
        attach.set_payload(open(path, 'rb').read())
        encode_base64(attach)
        basename = os.path.basename(path)
        attach.add_header('Content-Disposition', 'inline; filename="%s"' % basename)
        attach.add_header('Content-ID', '<%s>' % basename)
        attach.add_header('Content-Transfer-Encoding', 'base64')
        self.attach(attach)

    @staticmethod
    def get_template(template_params={}):
        render_params = {k: v for k, v in InviteEmailMessage.default_template_params.items()}
        for k, v in template_params.items():
            render_params[k] = v
        return get_temp(InviteEmailMessage.template_path).render(render_params)
