from django.conf.urls import url

from . import views

app_name = 'promo'
urlpatterns = [
    url(r'^$', views.PromoCodesListView.as_view(), name='promo_codes'),
    url(r'^codes/', views.PromoCodesListView.as_view(), name='promo_codes'),
    url(r'code/(?P<pk>[0-9]+)/', views.PromoCodeDetailView.as_view(), name='promo_code'),
    url(r'code/delete/(?P<pk>[0-9]+)', views.PromoCodeDelete.as_view(), name='delete_promo'),
    url(r'invite(?:/code/(?P<promo_code>\w*))?(?:/name/(?P<first_name>[\w ]*))?(?:/email/(?P<email>.*))?$', views.invite_view, name='invite'),
    url(r'invite/subscriber/(?P<subscriber_id>[0-9]*)', views.invite_view, name='invite'),
    url(r'^subscribers/', views.SubscribersView.as_view(), name='subscribers'),
]
