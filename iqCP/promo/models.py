# This is an auto-generated Django model module.
# You'll have to do the following manually to clean this up:
#   * Rearrange models' order
#   * Make sure each model has one field with primary_key=True
#   * Make sure each ForeignKey has `on_delete` set to the desired behavior.
#   * Remove `managed = True` lines if you wish to allow Django to create, modify, and delete the table
# Feel free to rename the models, but don't rename db_table values or field names.
#from __future__ import unicode_literals

from django.db import models
from django.core.urlresolvers import reverse

from users.models import Users

class PromoCodes(models.Model):
    code_id = models.AutoField(primary_key=True)
    code = models.CharField(unique=True, max_length=10)
    creation_time = models.DateTimeField(blank=True, null=True)
    used_time = models.DateTimeField(blank=True, null=True)
    comment = models.TextField(blank=True, null=True)
    subscriber = models.ForeignKey('SiteSubscribers', models.DO_NOTHING, blank=True, null=True)
    class Meta:
        managed = False
        db_table = 'promo_codes'

    def get_absolute_url(self):
        return reverse('promo:promo_code', kwargs={'pk': self.pk})


class UsersPromo(models.Model):
    user = models.ForeignKey(Users, models.DO_NOTHING)
    code = models.ForeignKey(PromoCodes, models.CASCADE)

    class Meta:
        managed = False
        db_table = 'users_promo'


class SiteSubscribers(models.Model):
    subscriber_id = models.AutoField(primary_key=True)
    email = models.CharField(max_length=100)
    first_name = models.CharField(max_length=100, blank=True, null=True)
    second_name = models.CharField(max_length=100, blank=True, null=True)
    ip = models.BigIntegerField(blank=True, null=True)
    time = models.DateTimeField(blank=True, null=True)
    job = models.CharField(max_length=1000, blank=True, null=True)
    class Meta:
        managed = False
        db_table = 'site_subscribers'