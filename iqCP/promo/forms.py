from django import forms

from accounts.models import Profile

#форма редактирования promo codes detail
class PromoCodeDetailForm(forms.Form):
    comment = forms.CharField(label='Комментарий')

#форма приглашения
class SendInviteForm(forms.Form):
    sender = forms.ModelChoiceField(queryset=Profile.objects.all(), label='От', required=True)
    recipient = forms.CharField(label='Email получателя', required=True)
    name = forms.CharField(label='Имя получателя',required=False)
    promo_code = forms.CharField(label='Промо-код', required=False)

