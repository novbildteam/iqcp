from django.shortcuts import render
from django.contrib.auth.decorators import login_required
from django import forms
from django.http import HttpResponseRedirect
from django.core.urlresolvers import reverse

from .models import Profile

SECURE_CHOICES = (
        (0, 'NONE'),
        (1, 'SSL')
    )

class ProfileForm(forms.ModelForm):
    class Meta:
        model = Profile
        fields = ['first_name', 'second_name', 'email', 'smtp_server', 'smtp_port', 'secure_type', 'email_password']
        widgets = {
            'first_name': forms.TextInput(),
            'second_name': forms.TextInput(),
            'email': forms.TextInput(),
            'smtp_server': forms.TextInput(),
            'secure_type': forms.Select(choices=SECURE_CHOICES),
            'email_password': forms.PasswordInput(render_value=True)
        }

@login_required()
def profile_view(request):
    try:
        profile = Profile.objects.get(user=request.user)
    except:
        return render(request, 'profile/profile_not_found.html')
    else:
        if request.method == 'POST':
            profile_form = ProfileForm(request.POST, instance=profile)
            if not profile_form.errors:
                profile_form.save()
                return HttpResponseRedirect(reverse('promo:promo_codes'))
        else:
            profile_form = ProfileForm(instance=profile)
    return render(request, 'profile/profile.html', {
        'profile_form': profile_form,
    })