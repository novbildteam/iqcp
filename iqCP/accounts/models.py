from django.db import models
from django.contrib.auth.models import User
# Create your models here.

class Profile(models.Model):
    first_name = models.TextField(blank=True, null=True)
    second_name = models.TextField(blank=True, null=True)
    user = models.ForeignKey(User, unique=True)
    email = models.TextField(null=False, default='')
    smtp_server = models.TextField(default='smtp.yandex.com')
    smtp_port = models.IntegerField(default=465)
    secure_type = models.SmallIntegerField(default=0)  # 0 - none secure; 1 - SSL
    email_password = models.CharField(max_length=255, null=True)

    def __str__(self):
        return self.email+' | '+self.first_name+' '+self.second_name+' '


# class ProfileEmailSettings(models.Model):
#     email = models.TextField(null=True)
#     smtp_server = models.TextField()
#     smtp_port = models.IntegerField()
#     secure_type = models.SmallIntegerField() #0 - none secure; 1 - SSL
#     user = models.ForeignKey(User, unique=True)
#
#     def __str__(self):
#         return self.email


