# -*- coding: utf-8 -*-
# Generated by Django 1.10.2 on 2016-11-02 12:52
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('accounts', '0003_auto_20161102_1212'),
    ]

    operations = [
        migrations.AddField(
            model_name='profile',
            name='email_password',
            field=models.CharField(max_length=255, null=True),
        ),
    ]
